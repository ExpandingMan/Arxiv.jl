# Arxiv

[![dev](https://img.shields.io/badge/docs-latest-blue?style=for-the-badge&logo=julia)](https://ExpandingMan.gitlab.io/Arxiv.jl/)
[![build](https://img.shields.io/gitlab/pipeline/ExpandingMan/Arxiv.jl/main?style=for-the-badge)](https://gitlab.com/ExpandingMan/Arxiv.jl/-/pipelines)

An unofficial Julia wrapper for the [arxiv.org API](https://info.arxiv.org/help/api/user-manual.html).

## Installation
```julia
] add Arxiv
```
Consider using [Term.jl](https://github.com/FedeClaudi/Term.jl) for nice previews.

## Preview
![](/docs/arxivjl_example.jpg)

