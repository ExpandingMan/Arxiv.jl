using Arxiv
using Documenter

DocMeta.setdocmeta!(Arxiv, :DocTestSetup, :(using Arxiv); recursive=true)

makedocs(;
    modules=[Arxiv],
    authors="ExpandingMan <savastio@protonmail.com> and contributors",
    repo="https://gitlab.com/ExpandingMan/Arxiv.jl/blob/{commit}{path}#{line}",
    sitename="Arxiv.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://ExpandingMan.gitlab.io/Arxiv.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
        "API" => "api.md",
    ],
)
