```@meta
CurrentModule = Arxiv
```

# API

```@index
```

## Main
```@docs
Id
Paper
@arXiv_str
Downloads.download
openwith
opensite
vanityurl
openvanity
printid
idstring
query
islatest
latest
info
```

## Internals
```@docs
Api
rawquery
```

