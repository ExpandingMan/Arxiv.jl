```@meta
CurrentModule = Arxiv
```

# Arxiv

[Arxiv.jl](https://gitlab.com/ExpandingMan/Arxiv.jl) is a simple wrapper of the [arxiv.org
API](https://info.arxiv.org/help/api/user-manual.html) to make it easy to fetch and view arxiv
papers and their metadata.


## Example
```julia
using Arxiv, Downloads

id = arXiv"2303.17626"  # create identifier from string

# this constructor takes the publication date (only needs to be accurate to month)
# version can also be specified
id = Arxiv.Id(Date(2023,3,1), 17626, version=1)
id = arXiv"2303.17626v1"  # with version, same as above

p = Paper(id)  # creation of a Paper object will download metadata

# if you have Term.jl loaded, you'll see a nice preview in terminal
# otherwise, you can also inspect the properties
p.title |> show
p.summary |> show  # the abstract
p.updated |> show  # last updated timestamp
p.published |> show  # published timestamp
p.comment |> show  # some metadata as a string, typically number of pages, versions
p.authors |> show  # authors as Vector{String}
p.doi |> show
p.primary_category |> show
p.categories |> show  # all categories as Vector{String}

# links if available
p.link_doi
p.link_text  # arxiv.org landing site
p.link_pdf

# searches return an array of Paper
Arxiv.query(search="einstein cartan gravity")
# or can be used to return multiple papers with a single remote call
Arxiv.query(ids=["2303.17626", "2304.00319"])  # args can be Id or string


Arxiv.opensite(p)  # opens the landing site for the paper in the browser

# open the PDF in the given program
Arxiv.openwith(p, "zathura")  # will be downloaded to temporary file

# can download using any of the usual options of Downloads.download
Downloads.download(p, "2303.17626.pdf")  # download to file

# open in browser via arxiv-vanity.com
Arxiv.openvanity(p)
```

## [Term.jl](https://github.com/FedeClaudi/Term.jl) Extension
If `Term` is loaded the `show` method for [`Paper`](@ref) will show a preview including the abstract
with most of the paper metadata.

## Future Additions
One of my original goals with this package was to be able to get arxiv metadata from existing PDF's.
This is possible because arxiv generates PDF's with predictable titles in the PDF metadata.
Unfortunately the PDF standard is horrific and tools for dealing with PDF files are relatively few
and far between.  Eventually I'd like to use [PDFIO.jl](https://github.com/sambitdash/PDFIO.jl) to
achieve this but, in the meantime, it is downgrading too many of my dependencies, so it needs to be
updated.
