module ArxivTermExt

using Arxiv, Term
using Arxiv: Id, Paper

function titlepanel(p::Paper)
    Panel(p.summary,
          style="yellow",
          title=p.title,
          title_style="italic green",
          box=:HORIZONTALS,
         )
end

function infopanel(p::Paper)
    authstr = join(p.authors, ", ")
    catstr = join(p.categories, ", ")
    Panel("{bold magenta}Authors:{/bold magenta} "*authstr,
          "{bold magenta}Categories:{/bold magenta} "*catstr,
          @style(p.comment, italic, yellow),
          title="{red}Published:{/red} "*string(p.published),
          box=:SIMPLE_HEAD,
         )
end

function Term.Panel(p::Paper)
    Panel(titlepanel(p),
          infopanel(p),
          title=string(p.id),
          style="magenta",
          title_style="bold cyan",
          subtitle=p.primary_category,
          subtitle_style="blue",
          width=100,
         )
end

Base.show(io::IO, mime::MIME"text/plain", p::Paper) = show(io, mime, Panel(p))

end
