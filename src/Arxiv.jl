module Arxiv

using Dates, Downloads
using HTTP, URIs, XMLDict

#TODO: would love to get info directly from PDF, but PDFIO is downgrading a lot

const DEFAULT_HOST = "export.arxiv.org"

"""
    Api

Data structure representing an arxiv API.  The default can be created with `Api()`.
Note that most functions that can take an `Api` as an argument can omit that argument to use the default.
"""
struct Api
    url::URI
end

const GLOBAL_API = Base.RefValue{Api}()

__init__() = (GLOBAL_API[] = Api())

function Api(host::AbstractString=DEFAULT_HOST;
             scheme::AbstractString="https",
             path::AbstractString="/api",
            )
    Api(URI(;host, scheme, path))
end


include("ids.jl")
include("queries.jl")
include("paper.jl")


export @arXiv_str

end
