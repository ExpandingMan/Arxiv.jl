
const RE_ID_PREFIX = r"\barXiv:"
const RE_ID_DATA = r"(\d\d)(\d\d)\.(\d{4,5})v?((?<=v)\d+)?\b"
const RE_ID_DATA_OLD = r"([\w-]*)/(\d\d)(\d\d)(\d{3})v?((?<=v)\d+)?\b"
const RE_ID = RE_ID_PREFIX * RE_ID_DATA
const RE_ID_OLD = RE_ID_PREFIX * RE_ID_DATA_OLD

"""
    Arxiv.Id

An arXiv reference identifier.
See [the identifier schema](https://info.arxiv.org/help/arxiv_identifier.html).

## Constructors
```julia
Id(y, m, n; version=0, legacy_category="")
Id(t::Date, n; version=0, legacy_category="")
Id(id::Id; version=0)

arXiv"2103.17058"  # the arXiv string macro can also be used
```

## Arguments
- `y`: The publication year as an integer.  E.g. `2023` and `23` are equivalent.
- `m`: The publication month as an integer.
- `n`: The ID number, e.g. this number for `2304.00319` is `2304.0319`.
- `version`: The version of the paper, `0` to indicate "latest".
- `legacy_category`: The category used by the old form arxiv identifiers.
- `id`: Another `Id` can be used to create a similar `Id` with a different version.
"""
struct Id
    year::Int
    month::Int
    number::Int  # note: arxiv *does* ignore padding, so the fact that this is a number is fine
    version::Int  # 0 for "latest"
    legacy_category::String
end

function Base.:(==)(id1::Id, id2::Id)
    id1.year == id2.year && id1.month == id2.month && id1.number == id2.number &&
        id1.version == id2.version && id1.legacy_category == id2.legacy_category
end

"""
    islatest(id::Id)

Checks whether the arxiv identifier is for the latest version.  This means that the identifier
*does not specify* the version, so that when API calls are made with this identifier, the arxiv
server interprets it as the latest version.  This will return `false` for a specific version that
happens to be the latest.
"""
islatest(id::Id) = id.version ≤ 0

function Id(y::Integer, m::Integer, n::Integer; version::Integer=0, legacy_category::AbstractString="")
    Id(y % 100, m, n, version, legacy_category)
end

Id(t::Date, n::Integer; kw...) = Id(year(t), month(t), n; kw...)

Id(id::Id; version::Integer=id.version) = Id(id.year, id.month, id.number, version, id.legacy_category)

"""
    latest(id::Id)

Returns the identifier for the latest version.  For example `latest(arXiv"2303.17626v3") == arXiv"2303.17626"`.
"""
latest(id::Id) = Id(id, version=0)

function _legacyid(str::AbstractString)
    m = match(RE_ID_DATA_OLD, str)
    isnothing(m) && throw(ArgumentError("invalid arXiv ID string: \"$str\""))
    v = m.captures[5]
    Id(parse(Int, m.captures[2]), parse(Int, m.captures[3]), parse(Int, m.captures[4]);
       version=(isnothing(v) ? 0 : parse(Int, v)),
       legacy_category=m.captures[1],
      )
end

function Id(str::AbstractString)
    m = match(RE_ID_DATA, str)
    isnothing(m) && return _legacyid(str)
    v = m.captures[4]
    Id(parse(Int, m.captures[1]), parse(Int, m.captures[2]), parse(Int, m.captures[3]);
       version=(isnothing(v) ? 0 : parse(Int, v))
      )
end

"""
    arXiv"1111.2222"

Creates an arxiv [`Id`](@ref) from a string.
"""
macro arXiv_str(str::String)
    Id(str)
end

function _print_version(io::IO, id::Id)
    id.version > 0 && print(io, 'v', id.version)
    nothing
end

function _print_old(io::IO, id::Id)
    print(io, id.legacy_category, '/', lpad(id.year % 100, 2, '0'), lpad(id.month, 2, '0'), lpad(id.number, 3, '0'))
end

"""
    printid([io::IO,] id::Id)

Print the identifier without the `arXiv:` prefix.
```julia
printid(arXiv"2103.17058")  # prints "2103.17058"
```
"""
function printid(io::IO, id::Id)
    if !isempty(id.legacy_category)
        _print_old(io, id)
    else
        print(io, lpad(id.year % 100, 2, '0'), lpad(id.month, 2, '0'), '.', lpad(id.number, 4, '0'))
    end
    _print_version(io, id)
end
printid(id::Id) = printid(stdout, id)

"""
    idstring(id::Id)

Returns a string giving the identifier without the `arXiv` prefix.
"""
idstring(id::Id) = sprint(printid, id)

function Base.print(io::IO, id::Id)
    print(io, "arXiv:")
    printid(io, id)
end

function Base.show(io::IO, id::Id)
    print(io, "arXiv")
    show(io, idstring(id))
end

