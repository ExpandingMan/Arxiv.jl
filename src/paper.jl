
"""
    Paper

A data structure representing an arxiv paper.  This contains all of the metadata on the paper
which can be obtained via the arxiv API, and can be used to easily download the paper.

## Properties
The following can be accessed with `getproperty`, e.g. `p.id`.
- `id`: The [`Id`](@ref) of the paper.
- `updated`: The timestamp of the most recent update.
- `published`: The timestamp of the publication.
- `title`: The title of the paper.
- `summary`: A summary description of the paper (usually the abstract).
- `authors`: A `Vector{String}` listing the paper's authors
- `doi`: The digital object identifier of the publication.
- `comment`: Usually this lists the number of pages and lists what was included in version changes.
- `primary_category`: A string giving the arxiv.org primary category of the paper.
- `categories`: A `Vector{String}` list of all arxiv.org categories to which the paper belongs.
- `link_doi`: The DOI in URL form, as a `URI` or `nothing`.
- `link_text`: The URL of the arxiv.org website for the paper, as a `URI` or `nothing`.
- `link_pdf`: The URL of the PDF download for the paper, as a `URI` or `nothing`.

## Constructors
All constructors generate an HTTP call to the host specified by `api` (i.e. arxiv.org).
```julia
Paper(api, id)
Paper(id)
```

## Arguments
- `api`: The [`Api`](@ref) for talking to arxiv.org.  Will use default if omitted (arxiv.org).
- `id`: An [`Id`](@ref) object with the identifier of the paper.
"""
struct Paper
    id::Id
    updated::DateTime
    published::DateTime
    title::String
    summary::String
    authors::Vector{String}
    doi::String
    comment::String
    primary_category::String
    categories::Vector{String}

    link_doi::Union{Nothing,URI}
    link_text::Union{Nothing,URI}
    link_pdf::Union{Nothing,URI}
end

function _datetime_notz(str::AbstractString)
    if endswith(str, "Z")
        (t, _) = rsplit(str, ':', limit=2)
        DateTime(t)
    else
        DateTime(str)
    end
end

function _get_authors(as)
    o = Vector{String}()
    for a ∈ as
        # the schema is rather unpredictable... xml
        if a isa Pair && a[1] == "name"
            push!(o, a[2])
        else
            aa = get(a, "name", nothing)
            isnothing(aa) || push!(o, aa)
        end
    end
    o
end

function _get_links(lks)
    ldoi = nothing
    ltxt = nothing
    lpdf = nothing
    for lk ∈ lks
        if get(lk, :title, nothing) == "doi"
            ldoi = URI(lk[:href])
        elseif get(lk, :type, nothing) == "text/html"
            ltxt = URI(lk[:href])
        elseif get(lk, :title, nothing) == "pdf"
            lpdf = URI(lk[:href])
        end
    end
    (ldoi, ltxt, lpdf)
end

_get_categories(c::AbstractArray) = map(x -> x[:term], c)
_get_categories(c::AbstractDict) = [c[:term]]

function Paper(dict::AbstractDict)
    (ldoi, ltxt, lpdf) = _get_links(dict["link"])
    Paper(Id(dict["id"]),
          _datetime_notz(dict["updated"]),
          _datetime_notz(dict["published"]),
          get(dict, "title", ""),
          get(dict, "summary", ""),
          _get_authors(get(dict, "author", [])),
          get(dict, "doi", ""),
          get(dict, "comment", ""),
          dict["primary_category"][:term],
          _get_categories(dict["category"]),
          ldoi, ltxt, lpdf,
        )
end

idstring(p::Paper) = idstring(p.id)

function Base.show(io::IO, p::Paper)
    show(io, typeof(p))
    print(io, "(")
    show(io, p.id)
    print(io, ")")
end

Paper(api::Api, id::Id) = Paper(info(api, id))
Paper(id::Id) = Paper(GLOBAL_API[], id)

"""
    download(p::Paper, out; kw...)

Downloads the paper `p` to `out` (e.g. a string or `IO`).  See the documentation for `Downloads.download`
for all available options.
"""
function Downloads.download(p::Paper, out; kw...)
    isnothing(p.link_pdf) && error("can't download $p as we don't have a download URL")
    Downloads.download(string(p.link_pdf), out; kw...)
end

"""
    openwith(p::Paper, cmd; wait=false, detach=true, kw...)

Download the PDF of the paper to a temporary file and open it with the command `cmd`.  `cmd` can either be a
string, array of strings or `Cmd`.  If `wait=true` execution will block until the process returns.
If `detach=true` the command will run in a new process group.  Additional keyword arguments are 
passed to `Downloads.download`.

## Examples
```julia
p = Paper(arxiv"2303.17626")
openwith(p, "rifle")
openwith(p, "zathura")
```
"""
function openwith(p::Paper, cmd::Cmd;
                  wait::Bool=false,
                  kw...
                 )
    path = tempname()*".pdf"  # extension helps programs to understand what kind of file this is
    Downloads.download(p, path; kw...)
    run(`$cmd $path`; wait)
    path
end
function openwith(p::Paper, cmd::AbstractVector{<:AbstractString}; detach::Bool=true, kw...) 
    openwith(p, Cmd(Cmd(cmd); detach); kw...)
end
openwith(p::Paper, cmd::AbstractString; kw...) = openwith(p, [cmd]; kw...)

"""
    opensite(p::Paper)

Open the landing site for the paper in a browser.

**NOTE:** This uses `xdg-open` and only works on linux.
"""
function opensite(p::Paper)
    isnothing(p.link_text) && error("can't open site for $p as we don't have a site URL")
    run(`xdg-open $(string(p.link_text))`)
end

"""
    vanityurl(p::Paper)

Get the URL for the `arxiv-vanity.com` site of the paper.
"""
vanityurl(p::Paper) = URI("https://www.arxiv-vanity.com/papers/$(idstring(p))/")

"""
    openvanity(p::Paper)

Open the paper in a browser in `arxiv-vanity.com`.

**NOTE:** This uses `xdg-open` and only works on linux.
"""
openvanity(p::Paper) = run(`xdg-open $(string(vanityurl(p)))`)

"""
    query([api::Arxiv.Api]; search="", ids=[], max_results=10, start=0)

Find papers with search string `search` with identifiers `ids`.  Each of `search`, `ids` will only
be used if non-empty.  The returned value is an array of [`Paper`](@ref) with maximum length
`max_results`.

`ids` should be an iterable of either [`Arxiv.Id`](@ref) or strings.
"""
function query(api::Api; search::AbstractString="", ids=[], kw...)
    q = rawquery(api; id_list=ids, search_query=search, kw...)
    rs = parse(Int, q["totalResults"]) == 1 ? [q["entry"]] : q["entry"]
    map(Paper, rs)
end
query(;kw...) = query(GLOBAL_API[]; kw...)

#TODO: bibtex reference?  don't really see an easy way of getting that
