
_idstring(id) = idstring(id)
_idstring(str::AbstractString) = str

"""
    rawquery(URI, api; search_query="", id_list=[], start=0, max_results=0)
    rawquery(HTTP.Response, api; kw...)
    rawquery(String, api; kw...)
    rawquery(api; kw...)

Performs a raw query of the arXiv search API with search arguments given by `search_query`,
`id_list`, `start` and `max_results`.
"""
function rawquery(::Type{URI}, api::Api;
                  search_query::AbstractString="",
                  id_list=[],
                  start::Integer=0,
                  max_results::Integer=0,
                 )
    q = Dict{String,String}()
    isempty(search_query) || (q["search_query"] = search_query)
    isempty(id_list) || (q["id_list"] = join(_idstring.(id_list), ","))
    start > 0 && (q["start"] = string(start))
    max_results > 0 && (q["max_results"] = string(max_results))
    URI(joinpath(api.url, "query"), query=q)
end
rawquery(::Type{HTTP.Response}, api::Api; kw...) = HTTP.get(rawquery(URI, api; kw...))
rawquery(::Type{String}, api::Api; kw...) = String(rawquery(HTTP.Response, api; kw...).body)
rawquery(api::Api; kw...) = parse_xml(rawquery(String, api; kw...))
rawquery(;kw...) = rawquery(GLOBAL_API[]; kw...)

"""
    info([api::Api], ids)

Get the information dictionaries for papers with `ids` in the iterable `ids`.
"""
function info(api::Api, ids)
    q = rawquery(api, id_list=ids)
    parse(Int, q["totalResults"]) == 1 ? [q["entry"]] : q["entry"]
end
info(ids) = info(GLOBAL_API[], ids)

info(api::Api, id::Union{Id,AbstractString}) = info(api, (id,))[1]
info(id::Union{Id,AbstractString}) = info(GLOBAL_API[], id)

