using Arxiv
using Dates
using Test

using Arxiv: Id, Paper


@testset "Arxiv.jl" begin
    @testset "id" begin
        id = Id(Date(2023,3,1), 17626)
        @test id.year == 23
        @test id.month == 3
        @test id.number == 17626
        @test id.version == 0
        @test isempty(id.legacy_category)
        @test Arxiv.islatest(id)
        @test string(id) == "arXiv:2303.17626"

        id1 = arXiv"2303.17626v3"
        @test id1.version == 3
        @test Arxiv.latest(id1) == id

        id = Id(2006, 4, 11, legacy_category="astro-ph")
        @test id.year == 6
        @test id.month == 4
        @test id.number == 11
        @test id.version == 0
        @test id.legacy_category == "astro-ph"
        @test string(id) == "arXiv:astro-ph/0604011"

        id = arXiv"hep-ph/1201567v2"
        @test id.year == 12
        @test id.month == 1
        @test id.number == 567
        @test id.version == 2
        @test id.legacy_category == "hep-ph"

        # can't use macro for error test because it'll happen at compile time
        @test_throws ArgumentError Id("1111.123456")
        @test_throws ArgumentError Id("bogus")
    end

    #WARN: the below makes exactly 1 call to arxiv.org
    @testset "paper" begin
        p = Paper(arXiv"2303.17626")
        @test Arxiv.latest(p.id) == arXiv"2303.17626"
        @test length(p.authors) == 2
        @test p.doi == ""
        @test p.published == DateTime(2023, 3, 30, 18)
        @test p.updated isa DateTime
        @test startswith(lowercase(p.comment), "5 pages")
        @test startswith(lowercase(p.title), "nuggets")
        @test startswith(lowercase(p.summary), "the lower limit")
        @test startswith(string(p.link_pdf), "http://arxiv.org/pdf/2303.17626")
        @test p.primary_category == "astro-ph.EP"
        @test Set(p.categories) == Set(["astro-ph.EP", "astro-ph.CO", "astro-ph.GA"])
    end
end
